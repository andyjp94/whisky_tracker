package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"regexp"
	"strconv"

	"github.com/anaskhan96/soup"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

func getAuction(c *gin.Context) {
	db, ok := c.MustGet("databaseConn").(*gorm.DB)
	if !ok {
		panic(ok)
	}
	stringID := c.Param("id")
	auctionID, err := strconv.Atoi(stringID)
	check(err)
	var lots []Lot
	err = db.Find(&lots, "auction_id=?", auctionID).Error
	check(err)
	c.JSON(200, lots)

}

func getWhisky(c *gin.Context) {
	db, ok := c.MustGet("databaseConn").(*gorm.DB)
	if !ok {
		panic(ok)
	}
	stringName := c.Param("name")
	var whisky Whisky
	db.Preload("Lots").First(&whisky, "name= ?", stringName)
	c.JSON(200, whisky)
}
func getLot(c *gin.Context) {
	db, ok := c.MustGet("databaseConn").(*gorm.DB)
	if !ok {
		panic(ok)
	}
	auctionIDString := c.Param("id")
	lotIDString := c.Param("lotID")
	auctionID, err := strconv.Atoi(auctionIDString)
	lotID, err := strconv.Atoi(lotIDString)
	var lot Lot
	err = db.Find(&lot, "auction_id=? AND lot_id=?", auctionID, lotID).Error
	check(err)
	c.JSON(200, lot)
}

func createAuction(c *gin.Context) {
	db, ok := c.MustGet("databaseConn").(*gorm.DB)
	if !ok {
		panic(ok)
	}

	stringID := c.Param("id")
	auctionID, err := strconv.Atoi(stringID)
	check(err)
	file, err := c.FormFile("file")

	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("get form err: %s", err.Error()))
		return
	}

	filename := filepath.Base(file.Filename)
	if err := c.SaveUploadedFile(file, filename); err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("upload file err: %s", err.Error()))
		return
	}
	fileContents, err := ioutil.ReadFile(filename)
	check(err)
	parsedHTML := soup.HTMLParse(string(fileContents))
	prodDesc := parsedHTML.FindAll("span", "class", "proddesc")
	for _, desc := range prodDesc {
		createLot(db, desc, auctionID, &Lot{})
	}
	c.String(http.StatusOK, fmt.Sprintf("File %s uploaded successfully.", file.Filename))
}

func getCurrencyAndValue(cost string) (currency string, value float64) {
	basicMatch := "\\d+\\.\\d\\d"
	poundMatch, err := regexp.Compile("£" + basicMatch)
	check(err)
	dollarMatch, err := regexp.Compile("\\$" + basicMatch)
	check(err)
	if poundMatch.MatchString(cost) {
		currency = "GBP"

	} else if dollarMatch.MatchString(cost) {
		currency = "USD"
	}
	//Convert to a rune to properly handle the symbol
	value, err = strconv.ParseFloat(string([]rune(cost)[1:]), 64)
	check(err)

	return
}

func getLotNumber(lotSoup soup.Root) (lotNumber int) {
	//This matches the lot string up until the point where it would match the unique
	//lot number. We then match against that to get the last index that this generic
	//string goes up to. That way we know the staring index for the lot number
	latestLotMatcher, err := regexp.Compile("\\w+\\s\\w+:\\s\\d+-")
	check(err)
	lotString := lotSoup.FindStrict("span", "class", "prodlot priceline curprice").FullText()
	index := latestLotMatcher.FindStringIndex(lotString)[1]
	lotNumber, err = strconv.Atoi(lotString[index:])
	check(err)

	return
}

func createWhisky(db *gorm.DB, lot soup.Root, record Lot, whisky *Whisky) bool {
	title := lot.Find("span", "class", "prodtitle").FullText()
	recordNotFound := db.First(whisky, "Name=?", title).RecordNotFound()
	if recordNotFound {
		*whisky = Whisky{Name: title, Lots: []Lot{record}}
		err := db.Create(whisky).Error
		check(err)
		return false
	}
	return true
}

func getSaleDetails(lotSoup soup.Root) (currency string, price float64, sold bool) {

	priceSoup := lotSoup.Find("span", "class", "price")
	//Can't improve the error handling here because of a bug in v2.0 release
	//https://github.com/anaskhan96/soup/issues/50
	if priceSoup.Error != nil {
		price = 0.00
		currency = ""
		sold = false
	} else {
		stringPrice := priceSoup.Text()
		currency, price = getCurrencyAndValue(stringPrice)
		sold = true
	}
	return
}

func createLot(db *gorm.DB, lotSoup soup.Root, auctionID int, lot *Lot) bool {
	lotNumber := getLotNumber(lotSoup)
	recordExists := !db.Where("lot_id = ? and auction_id = ?", lotNumber, auctionID).First(&Lot{}).RecordNotFound()
	if recordExists {
		return false
	}
	currency, price, sold := getSaleDetails(lotSoup)
	*lot = Lot{AuctionID: auctionID, LotID: lotNumber, Sold: sold, Price: price, Currency: currency}
	var whisky Whisky
	if !createWhisky(db, lotSoup, *lot, &whisky) {
		err := db.Model(&whisky).Association("Lots").Append(*lot).Error
		check(err)
	}
	return true
}
