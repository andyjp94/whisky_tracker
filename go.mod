module gitlab.com/andyjp94/whisky

go 1.14

require (
	github.com/anaskhan96/soup v1.1.1
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/protobuf v1.4.1 // indirect
	github.com/jinzhu/gorm v1.9.12
	github.com/lib/pq v1.5.1 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/spf13/viper v1.7.1
	golang.org/x/net v0.0.0-20200505041828-1ed23360d12c // indirect
	golang.org/x/sys v0.0.0-20200501145240-bc7a7d42d5c3 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
