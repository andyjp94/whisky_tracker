FROM golang:1.14-alpine AS build

RUN mkdir /whisky
WORKDIR /whisky
COPY . ./ 
ENV CGO_ENABLED 0
RUN go build -o /bin/whisky
FROM scratch
COPY --from=build /bin/whisky /bin/whisky
COPY --from=build /whisky/config.yml /etc/config.yml
CMD ["/bin/whisky"]