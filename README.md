# Whisky Tracker
The reason I am writing this document is that I originally just went for it and got myself into a mess with DynamoDB.

The whisky tracker retrieves information from online auctions about the sale of Whiskys in order to select what Whiskys are best to buy and potentially other useful trends.

## Usage
### Natively Using this package
You'll need a database running with the same credentials as you have set in the config file for dev. Code to do this with helm will be added shortly.
Once you have postgres running with the same configuration as the config file then run:
```
go run main.go models.go auctions.go
```

### Docker
You can either build the docker image locally or you can pull the docker image from the docker [hub](https://hub.docker.com/repository/docker/andyjp94/whisky_tracker).

### Configuration
No matter how you run the app you can override any value in the config file by creating an environment variable with the same name as the config key but capitalised and prefixed with `WHISKY_`. For further information please refer to the Viper libraries [documentation](https://github.com/spf13/viper).

## Design

This will be a three tier application

### Frontend
The frontend will have two purposes:
1. A dashboard view to track the performance of whiskies over time. Interesting dashboards may be:
    1. Individual whisky price over time
    2. Region Price over time
    3. Growth in the overall market
    4. Distillery trends
2. An update view to add useful info to the data which will not be able to be gathered, this should either be ad hoc or via rules. An example of a rule being any Whisky that contains the name Johnnie Walker should be designated have its Distillery set to Johnnie Walker.

The Framework for the Frontend will make use of the Vue framework and will be deployed using Netlify.


### Backend
The Backend will be a REST api that should be used to perform CRUD operations upon the DB and to parse the information from the website. It will be written in Go because it is my favourite language and the libraries to enable this to work. This will be deployed on Kubernetes on digital Ocean. This is because Kubernets is a superb way to deploy a containerised application and I need to learn more about it. To avoid hammering the website the files will only be retrieved once and then stuck in a data store, very likely s3.


### Database
The database will be a Postgres database. I toyed with this being NoSQL to save on cost for various reasons this data set isn't appropriate. This will probably be on RDS. The Database tables I will use are:

#### Tables
##### Auction
number - integer
start_date - datetime object
end_date - datetime object 
lots - one to many with Lot table

##### Lot
number - integer
sold - boolean
price - float
Whisky - many to one with Whisky Table

##### Whisky
name - string
Distillery - string
Country - string
Region - string
Year - datetime object

##### Rule
match - string
type - string
action - json {
    Distillery - string
    Country - string
    Region - string
    Year - Datetime object
}

### Tasks
1. Define the database tables
2. Create function to create a lot
3. Create function to create an auction.

