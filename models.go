package main

import (
	"time"

	"github.com/jinzhu/gorm/dialects/postgres"
)

type Lot struct {
	AuctionID int     `gorm:"primary_key;auto_increment:false"`
	LotID     int     `gorm:"primary_key;auto_increment:false"`
	Sold      bool    `gorm:"not null"`
	Price     float64 `gorm:"not null"`
	Currency  string  `gorm:"not null"`
	WhiskyID  string
	Date      time.Time
}

type Whisky struct {
	Name       string `gorm:"primary_key"`
	Distillery string
	Country    string
	Region     string
	Year       time.Time
	Lots       []Lot `gorm:"foreignkey:WhiskyID"`
}

type MatchMethod int

const (
	Contains MatchMethod = iota
	Begins
	Ends
)

func (m MatchMethod) String() string {
	names := []string{
		"Contains",
		"Begins",
		"Ends"}
	if m < Contains || m > Ends {
		return "Unknown"
	}
	// constant from the names array
	// above.
	return names[m]
}

type Rule struct {
	Name   string         `gorm:"unique;not null"`
	Match  string         `gorm:"unique;not null"`
	method MatchMethod    `gorm:"not null"`
	Fields postgres.Jsonb `gorm:"not null"`
}
